#!/bin/bash

#egemmain:
screen -dmS egemgeth /home/riddlez666/go-egem-rb/build/bin/egem --rpc --rpcapi=eth,net,web3 --maxpeers 1000 --unlock="0x5080fb28d8cf96c320e1a2e56a901abb7391b4ce" --password="/home/riddlez666/egempass" --allow-insecure-unlock --mine --identity "pool.egem.io" --extradata "EGEM Dev Pool"

sleep 5

#pool1b:
screen -dmS egempool1b /home/riddlez666/open-egem-pool/build/bin/open-ethereum-pool /home/riddlez666/pool1b.json

sleep 5

#pool2b:
screen -dmS egempool2b /home/riddlez666/open-egem-pool/build/bin/open-ethereum-pool /home/riddlez666/pool2b.json

sleep 5

#pool3b:
screen -dmS egempool3b /home/riddlez666/open-egem-pool/build/bin/open-ethereum-pool /home/riddlez666/pool3b.json

sleep 5

#pool4b:
screen -dmS egempool4b /home/riddlez666/open-egem-pool/build/bin/open-ethereum-pool /home/riddlez666/pool4b.json

sleep 5

#pool6b:
screen -dmS egempool6b /home/riddlez666/open-egem-pool/build/bin/open-ethereum-pool /home/riddlez666/pool6b.json

sleep 5

#pool42b:
screen -dmS egempool42b /home/riddlez666/open-egem-pool/build/bin/open-ethereum-pool /home/riddlez666/pool42b.json

sleep 5

#pool9b:
screen -dmS egempool9b /home/riddlez666/open-egem-pool/build/bin/open-ethereum-pool /home/riddlez666/pool9b.json

sleep 5

#api:
screen -dmS egemapi /home/riddlez666/open-egem-pool/build/bin/open-ethereum-pool /home/riddlez666/api.json

sleep 5

#unlocker:
screen -dmS egemunlocker /home/riddlez666/open-egem-pool/build/bin/open-ethereum-pool /home/riddlez666/unlocker.json

sleep 5

#payout:
#screen -dmS egempayout /home/riddlez666/open-egem-pool/build/bin/open-ethereum-pool /home/riddlez666/payout.json

sleep 5

exit 0